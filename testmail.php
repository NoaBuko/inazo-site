<?php

    $email_valid = false;
    if (isset($_POST['user_mail']))
    {
        $_POST['user_mail'] = htmlspecialchars($_POST['user_mail']); // On rend inoffensives les balises HTML que le visiteur a pu rentrer

        if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['user_mail']))
        {
            $email_valid = true;
            //echo 'L\'adresse ' . $_POST['user_mail'] . ' est <strong>valide</strong> !';
        }
    }

    if ($email_valid) {
        ini_set("sendmail_path", "/usr/sbin/sendmail -t -i");
        
        $to = "noabukovec@gmail.com";
     
        $from = htmlspecialchars($_POST['user_mail']); 
        
        $subject = htmlspecialchars($_POST['user_sujet']);
     
        $message = htmlspecialchars($_POST['user_message']);
     
        $headers = "From:" . $from;
     
        $result = mail($to,$subject,$message, $headers);
    }
 ?>
<?php include "header.php" ?>
        <div>
            <?php if ($email_valid) { ?>
                <span>L'email <?php echo (($result == true) ? "a été" : "n'a pas été") ?> envoyé</span>
            <?php } else { ?>
                <span>L'email saisi n'est pas valide</span>
            <?php } ?>
        </div>
        <a class="aaa" href="/">Retour à la page d'accueil</a>
<?php include "footer.php" ?>
