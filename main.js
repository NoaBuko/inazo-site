const slider2 = document.querySelector('#slider2');

slider2.addEventListener('click', function() {
	const images = [
		'Images/logo.jpg',
		'Images/minia1.jpg',
		'Images/minia2.jpg',
		'Images/minia3.jpg',
		'Images/minia4.jpg'
	]
	
	const mami = slider2.getAttribute('src')
	
	let index = images.findIndex(function(image) {
		return mami === image
	})
	
	if (index === images.length - 1) {
		index = -1
	}
	
	slider2.setAttribute('src', images[index + 1])
})
