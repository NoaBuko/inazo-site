<!DOCTYPE html>
<html> 
	<head>
		<meta charset="utf-8"/>
		<title>Inazo</title>
		<link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou" rel="stylesheet" type="text/css">
		<link href="styles.css" rel="stylesheet" type="text/css">
		<style> 
			form {
				margin: 0 auto;
				width: 500px;
				padding: 3em;
				border: 1px solid #CCC;
				border-radius: 3em;
				}

				form div + div {
					margin-top: 3em;
				}

			label {
				display: inline-block;
				width: 90px;
				text-align: right;
				font-size: 20px;
				color: orange;
			}

			input, textarea {
				font: 3em sans-serif;

				width: 400px;
				box-sizing: border-box;
				
				border: 1px solid #999;
			}

			input:focus, textarea:focus {
				border-color: #000;
			}

			textarea {
				vertical-align: top;

				height: 8em;
			}

			.button {
				padding-left: 100px; 
			}

			button {
				margin-left: .7em;
			}
		</style>
		
		<script type="text/javascript">
               function nb_aleatoire(min, max)
          {
               var nb = min + (max-min+1)*Math.random();
               return Math.floor(nb);
          }
               
               function PoM_manche(min, max)
          {
               var nb = nb_aleatoire(min, max);
               var cpt = 0;
               var saisie;
               var msg = 'Le nombre a deviner est compris entre ' + min + ' et ' + max + '.';

               do
               {
                    saisie = prompt(msg);
                    if(saisie == null)
                         return 0;

                    cpt++;
                    if(saisie > nb)
                         msg = "C'est moins";
                    else
                         msg = "C'est plus";
               }
               while(saisie != nb);

               return cpt;
          }
          
          
               function PoM_partie(min, max)
          {
               var cpt = 0;
               var best_score = 0;
               var score;
               var continuer;

               do
               {
                    score = PoM_manche(min, max);
                    if(score)
                    {
                         cpt++;
                         if(score < best_score || best_score == 0)
                              best_score = score;
                         continuer = confirm("Bravo, tu as gagne en " + score + " coups.\nVeux-tu rejouer ?");
                    }
                    else
                         continuer = false;
               }
               while(continuer);

               alert("Tu as joue " + cpt + " manche(s).\nTon meilleur score est de " + best_score + " coups.");
               return best_score;
          }

          </script>
	</head>

	<body>
		<div class="div_headers">
			<table>
				<tr>
					<td class="header_title_cell">
						<h1><a class="h1head" href="/">Inazo: le site</a></h1>
					</td>
					<td class="margin">
						<a class="header_cell_link" href="presentation.php"><h2>Présentation</h2></a>
					</td>
					<td class="margin">
						<a class="header_cell_link" href="videos.php"><h2>Vidéos</h2></a>
					</td>
					<td class="margin">
						<a class="header_cell_link" href="jeu.php"><h2>Jeu</h2></a>
					</td>	
				</tr>
			</table>
		</div>
		
			
