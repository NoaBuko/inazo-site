<?php include "header.php" ?>
<p>Voici l'espace où vous pouvez acceder à toutes mes vidéos. parfois il y aura des vidéos dont le lien ne sera que sur cette page.</p>
<p>Ce qui veut dire que seul les personnes qui regarderont ce site pourront voir la vidéos.</p>

<h2 class="h2_index">Mes dernières vidéos</h2>
<p>Cliquez sur les titres suivants pour accéder à mes dernières vidéos:</p>

<ul>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=xq8tnbBoyaw">La véritable identité d'Ichihoshi !? Reviewfoot 22</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=r1UWeb8abzU">C'en est trop !? Reviewfoot 21</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=AhvBJ-fodbk">Analyse du synopsis de l'épisode 11 d'Inazuma Eleven Orion !?</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=ruEGzUii8Ns">Joyeux Noël!!!! Ichihoshi pleure enfin !?#Calendrierdelavent</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=GtvtRBM7FNQ">Interview de Toriyama-sensei et de Toyotaro !? #Calendrierdelavent</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=11k5tAz8t_o">Le puissant Moro !? Chapitre 43 #Calendrierdelavent</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=KCwoDlXPBiA">Le retour de l'ultra instinct !? #Calendrierdelavent</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=byCJ1kDsPXM">L'histoire de Nosaka Yuuma !? #Calendrierdelavent</a></li>
	<li><a class="aaa" target="_blank" href="https://www.youtube.com/watch?v=LHLv7vdsLdE">Synopsis de l'épisode 10 d'Inazuma Eleven Orion !? #Calendrierdelavent</a></li>
	
</ul>

<img id="slider2" class="slider_slider2" src="Images/logo.jpg" alt="Ma chaine">

<?php include "footer.php" ?>
